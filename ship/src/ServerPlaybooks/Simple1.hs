{-# LANGUAGE OverloadedStrings #-}

module ServerPlaybooks.Simple1
    ( simple1
    ) where

import Shelly
import Automaton

import System.DevOps.ServerPlaybook
import System.DevOps.ServerPlaybook.Operation
import System.SystemAdmin.Ssh
import System.SystemAdmin.Users
import System.VaultSecrets.SshKey

simple1 :: ServerPlaybook
simple1 = mkServerPlayBook "simple1" "Simple1" vaultCheckout operations

vaultCheckout :: VaultCheckout ()
vaultCheckout = VaultCheckout
    emptyContext
    emptyFinally
    [ VaultCheckoutItem (VaultSecretPath "secret/ssh-keys/gitlab-captain") $ \_ targetDir sshKey -> do
        saveSshKeyToFile sshKey "gitlab-captain" targetDir
    ]

operations :: [Operation Shelly.FilePath]
operations =
    [ disableSshPasswordAuth

    , operation "Say hi!" $ \_ -> do
        echo "hi"

    , operation "Create user 'gameserver'" $ \_ -> do
        unlessM (userExists "gameserver") $
            userAdd "gameserver"
    ]
