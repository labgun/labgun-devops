{-# LANGUAGE OverloadedStrings #-}

module ServerPlaybooks.WebServer
    ( webServer
    ) where

import Automaton
import Data.Aeson (Object)
import Data.Monoid ((<>))
import Data.Text
import Shelly
import System.DevOps.ServerPlaybook
import System.DevOps.ServerPlaybook.Operation
import System.SystemAdmin.Apt
import System.SystemAdmin.Ssh
import System.SystemAdmin.Users
import System.VaultSecrets.AwsCredentials
import Text.EDE
import qualified Data.Text as T
import qualified Data.Text.Lazy.IO as T

import ServerPlaybooks.WebServer.FacebookCredentials

multicaveSiteVersion :: Text
multicaveSiteVersion = "multicave-site-0.0.0.1.tar.xz"

-- This playbook currently requires that you manually setup the database after
-- it is run:
--
--     # sudo -u postgres psql
--
--     postgres=# CREATE USER dbuser WITH PASSWORD 'ZCM164unV86f';
--
--     postgres=# CREATE DATABASE multicave WITH
--                    OWNER=dbuser
--                    TEMPLATE=template0
--                    ENCODING='UTF8'
--                    LC_COLLATE='en_US.utf8'
--                    LC_CTYPE='en_US.utf8';
--
-- and then you must also manually initialize multicave-site:
--
--     # cd /srv/multicave-site; sudo -u multicave ./multicave-server initdb -c config.yaml
--     # sudo -u multicave /srv/multicave-site/multicave-server createsuperuser -c /srv/multicave-site/config.yaml
--
-- The service might not have been able to start correctly automatically so
-- manually restart it:
--
--     # systemctl restart multicave-site.service

webServer :: ServerPlaybook
webServer = mkServerPlayBook "web-server" "WebServer" vaultCheckout operations

s3bucket :: Text
s3bucket = "multicave-site-dist"

vaultCheckout :: VaultCheckout ()
vaultCheckout = VaultCheckout
    emptyContext
    emptyFinally
    [ VaultCheckoutItem (VaultSecretPath "secret/aws-credentials/webserver-init") $ \_ targetDir awsCredentials -> do
        saveAwsCredentials awsCredentials (targetDir </> awsCredentialsFile)
    , VaultCheckoutItem (VaultSecretPath "secret/multicave/facebook-app-credentials") $ \_ targetDir facebookCredentials -> do
        saveFacebookCredentials facebookCredentials (targetDir </> facebookCredentialsFile)
    ]

awsCredentialsFile :: Shelly.FilePath
awsCredentialsFile = "aws.json"

facebookCredentialsFile :: Shelly.FilePath
facebookCredentialsFile = "facebook.json"

templatesDir :: Shelly.FilePath
templatesDir = "templates"

operations :: [Operation Shelly.FilePath]
operations =
    [ disableSshPasswordAuth

    , operation "Update System Software" $ \_ -> do
        aptUpdate
        aptUpgrade

    , operation "Install AWS CLI program" $ \_ -> do
        aptInstall ["awscli"]

    , operation "Install multicave-site package" $ \secretsDir -> do
        awsCredentials <- loadAwsCredentials (secretsDir </> awsCredentialsFile)
        sub $ do
            setenv "AWS_ACCESS_KEY_ID" (awsAccessKey awsCredentials)
            setenv "AWS_SECRET_ACCESS_KEY" (awsSecretKey awsCredentials)
            run_ "aws" ["s3", "cp", "s3://" <> s3bucket <> "/" <> multicaveSiteVersion, "/tmp/" <> multicaveSiteVersion]

        rm_rf "/srv/multicave-server"
        run_ "tar" ["xf", "/tmp/" <> multicaveSiteVersion, "-C", "/srv"]
        rm (("/tmp" :: Text) </> multicaveSiteVersion)

        facebookCredentials <- loadFacebookCredentials (secretsDir </> facebookCredentialsFile)

        let env = fromPairs [ "facebookCredentials" .= facebookCredentials ]
        renderEdeTemplate "webserver/multicave-server/config.yaml" env "/srv/multicave-site/config.yaml"

    , operation "Install PostgreSQL" $ \_ -> do
        aptInstall ["postgresql"]

    , operation "Install nginx" $ \_ -> do
        aptInstall ["nginx"]

    , operation "Configure nginx" $ \_ -> do
        cp (templatesDir </> ("webserver/nginx/default" :: Text)) "/etc/nginx/sites-available/default"
        run_ "service" ["nginx", "reload"]

    , operation "Create user 'multicave'" $ \_ -> do
        unlessM (userExists "multicave") $
            userAdd "multicave"

    , operation "Install multicave-site.service" $ \_ -> do
        cp (templatesDir </> ("webserver/systemd/multicave-site.service" :: Text)) "/etc/systemd/system/multicave-site.service"

        -- Make sure that systemd notices the new service file that we just
        -- added(or changed):
        run_ "systemctl" ["daemon-reload"]

        -- Tell systemd to start the service on boot:
        run_ "systemctl" ["enable", "multicave-site.service"]

        -- If a previous version of the service was already running, then
        -- restart it so it gets the new version. If there was no previous
        -- version, then this will start the service:
        run_ "systemctl" ["restart", "multicave-site.service"]

    ]

renderEdeTemplate :: Shelly.FilePath -> Object -> Shelly.FilePath -> Sh ()
renderEdeTemplate filename env outfile = do
    let tmplFile = T.unpack (toTextIgnore (templatesDir </> filename))
    r <- liftIO $ eitherParseFile tmplFile
    configYamlTemplate <- either fail pure r

    rendered <- either fail pure (eitherRender configYamlTemplate env)
    liftIO $ T.writeFile (T.unpack (toTextIgnore outfile)) rendered
