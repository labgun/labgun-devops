{-# LANGUAGE OverloadedStrings #-}

module ServerPlaybooks.DevServer
    ( devServer
    ) where

import Automaton
import Data.Monoid ((<>))
import Data.Text
import Shelly
import System.DevOps.ServerPlaybook
import System.DevOps.ServerPlaybook.Operation
import System.SystemAdmin.Apt
import System.SystemAdmin.Ssh
import System.SystemAdmin.Users
import System.VaultSecrets.AwsCredentials

devServer :: ServerPlaybook
devServer = mkServerPlayBook "dev-server" "DevServer" vaultCheckout operations

s3bucket :: Text
s3bucket = "multicave-devserver-files"

vaultCheckout :: VaultCheckout ()
vaultCheckout = VaultCheckout
    emptyContext
    emptyFinally
    [ VaultCheckoutItem (VaultSecretPath "secret/aws-credentials/devserver-init") $ \_ targetDir awsCredentials -> do
        saveAwsCredentials awsCredentials (targetDir </> awsCredentialsFile)
    ]

awsCredentialsFile :: Shelly.FilePath
awsCredentialsFile = "aws.json"

templatesDir :: Shelly.FilePath
templatesDir = "templates"

operations :: [Operation Shelly.FilePath]
operations =
    [ disableSshPasswordAuth

    , operation "Update System Software" $ \_ -> do
        aptUpdate
        aptUpgrade

    , operation "Install AWS CLI program" $ \_ -> do
        aptInstall ["awscli"]

    , operation "Install DevServer packages" $ \secretsDir -> do
        awsCredentials <- loadAwsCredentials (secretsDir </> awsCredentialsFile)
        sub $ do
            setenv "AWS_ACCESS_KEY_ID" (awsAccessKey awsCredentials)
            setenv "AWS_SECRET_ACCESS_KEY" (awsSecretKey awsCredentials)
            run_ "aws" ["s3", "cp", "s3://" <> s3bucket <> "/springy-latest.web.tar.xz", "/tmp/springy-latest.web.tar.xz"]
            run_ "aws" ["s3", "cp", "s3://" <> s3bucket <> "/springy-latest.server.tar.xz", "/tmp/springy-latest.server.tar.xz"]
            run_ "aws" ["s3", "cp", "s3://" <> s3bucket <> "/oxala-mediator-latest.tar.xz", "/tmp/oxala-mediator-latest.tar.xz"]

        rm_rf "/srv/springy-latest.web"
        run_ "tar" ["xf", "/tmp/springy-latest.web.tar.xz", "-C", "/srv"]

        rm_rf "/srv/springy-latest.server"
        run_ "tar" ["xf", "/tmp/springy-latest.server.tar.xz", "-C", "/srv"]

        rm_rf "/srv/oxala-mediator-latest"
        run_ "tar" ["xf", "/tmp/oxala-mediator-latest.tar.xz", "-C", "/srv"]

        rm "/tmp/springy-latest.web.tar.xz"
        rm "/tmp/springy-latest.server.tar.xz"
        rm "/tmp/oxala-mediator-latest.tar.xz"

    , operation "Install nginx" $ \_ -> do
        aptInstall ["nginx"]

    , operation "Configure nginx" $ \_ -> do
        cp (templatesDir </> ("devserver/nginx/default" :: Text)) "/etc/nginx/sites-available/default"
        run_ "service" ["nginx", "reload"]

    , operation "Create user 'gameserver'" $ \_ -> do
        unlessM (userExists "gameserver") $
            userAdd "gameserver"

    , operation "Install oxala-mediator.service" $ \_ -> do
        cp (templatesDir </> ("devserver/systemd/oxala-mediator.service" :: Text)) "/etc/systemd/system/oxala-mediator.service"

        -- Make sure that systemd notices the new service file that we just
        -- added(or changed):
        run_ "systemctl" ["daemon-reload"]

        -- Tell systemd to start the service on boot:
        run_ "systemctl" ["enable", "oxala-mediator.service"]

        -- If a previous version of the service was already running, then
        -- restart it so it gets the new version. If there was no previous
        -- version, then this will start the service:
        run_ "systemctl" ["restart", "oxala-mediator.service"]

    ]
