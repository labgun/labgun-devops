{-# LANGUAGE OverloadedStrings #-}

module ServerPlaybooks.Kibbles
    ( kibbles
    ) where

import Shelly
import qualified Data.Text as T
import Data.Text (Text)
import Automaton

import System.SystemAdmin.Apt
import System.SystemAdmin.Ssh
import System.SystemAdmin.SystemSwap
import System.SystemAdmin.Users
import System.VaultSecrets.SshKey
import System.VaultSecrets.SshKey.FamousHosts
import System.DevOps.ServerPlaybook
import System.DevOps.ServerPlaybook.Operation

kibbles :: ServerPlaybook
kibbles = mkServerPlayBook "kibbles" "Do some fun stuff with kibbles" kibblesVaultCheckout kibblesOperations

kibblesVaultCheckout :: VaultCheckout ()
kibblesVaultCheckout = VaultCheckout
    emptyContext
    emptyFinally
    [ VaultCheckoutItem (VaultSecretPath "secret/ssh-keys/gitlab-captain") $ \_ targetDir sshKey -> do
        saveSshKeyToFile sshKey "gitlab-captain" targetDir
    ]

kibblesOperations :: [Operation Shelly.FilePath]
kibblesOperations =
    [ disableSshPasswordAuth

    , operation "Enable System Swap" $ \_ -> do
        let swapSizeMB = 2048
        enableSystemSwap swapSizeMB

    , operation "Update System Software" $ \_ -> do
        aptUpdate
        aptUpgrade

    , operation "Install GHC" $ \_ -> do
        aptAddRepository "ppa:hvr/ghc"
        aptUpdate
        aptInstall
            [ "ghc-" `T.append` ghcVersion
            , "cabal-install-" `T.append` cabalVersion

            -- Installing happy and alex with cabal is frustrating, much easier
            -- to do it here.
            --
            -- Difficulties when trying to use cabal:
            --
            --   Doesn't work with "cabal new-build" because both happy and
            --   alex are hardcoded to use "dist" directory, while "new-build"
            --   uses "dist-newstyle":
            --
            --   https://github.com/commercialhaskell/stack/issues/157
            --
            --   Even if you manage to work around above issue, the built
            --   executable has hard-coded paths that don't play well with
            --   "cabal new-build":
            --
            --   https://github.com/fpco/minghc/issues/24
            , "happy-" `T.append` happyVersion
            , "alex-" `T.append` alexVersion

            -- dev libraries that are needed for various cabal dependencies
            , "build-essential"
            , "zlib1g-dev"
            ]

    , operation "Create user captain" $ \_ -> do
        unlessM (userExists "captain") $ do
            userAdd "captain"
            let pathVar = makePathVar
                    [ "/home/captain/bin"
                    , "/opt/ghc/" `T.append` ghcVersion `T.append` "/bin"
                    , "/opt/cabal/" `T.append` cabalVersion `T.append` "/bin"
                    , "/opt/happy/" `T.append` happyVersion `T.append` "/bin"
                    , "/opt/alex/" `T.append` alexVersion `T.append` "/bin"
                    , "$PATH"
                    ]
                pathLine = T.concat [ "PATH=", pathVar, "\n"]
            appendfile "/home/captain/.profile" pathLine

    , operation "Configure SSH for captain" $ \secretsDir -> do
        installSshKey secretsDir "gitlab-captain" "captain"
        addToSshKnownHosts gitlabHostKeys "captain"

    , operation "Initialize cabal for captain" $ \_ -> do
        run_ "sudo" $ ["-u", "captain", "-i"] ++ ["cabal", "update"]

    , installCabalProgram "captain" "hprotoc"

    , operation "Download repo" $ \_ -> do
        aptInstall ["git"]
        run_ "sudo" $ ["-u", "captain", "-i"] ++ ["git", "clone", "--depth", "1", "git@gitlab.com:bitc/labgun.git"]

    , operation "Build repo" $ \_ -> do
        verbosely $ run_ "sudo" $ ["-u", "captain", "-i"] ++ ["bash", "-c", "cd labgun ; cabal new-build labgun-server lgmode-haskell-simple"]
    ]

installCabalProgram :: Text -> Text -> Operation a
installCabalProgram user prog = operation ("Install cabal program: " `T.append` prog) $ \_ -> do
    let tmpDir = "tmp-cabal"
    bracket_sh
        (run_ "sudo" $ ["-u", user, "-i"] ++ ["mkdir", tmpDir])
        (const $ run_ "sudo" $ ["-u", user, "-i"] ++ ["rm", "-rf", tmpDir])
        (const $ install tmpDir)
    where
    install tmpDir = do
        run_ "sudo" $ ["-u", user, "-i"] ++ ["bash", "-c", "cd " `T.append` tmpDir `T.append` " ; cabal get " `T.append` prog]

        let pkgDir = tmpDir `T.append` "/" `T.append` prog `T.append` "-*"

        run_ "sudo" $ ["-u", user, "-i"] ++ ["bash", "-c", "cd " `T.append` pkgDir `T.append` " ; cabal new-build"]
        run_ "sudo" $ ["-u", user, "-i"] ++ ["mkdir", "-p", "bin"]
        run_ "sudo" $ ["-u", user, "-i"] ++ ["bash", "-c",
              "mv " `T.append`
              pkgDir `T.append` "/dist-newstyle/build/" `T.append` prog `T.append` "-*/build/" `T.append` prog `T.append` "/" `T.append` prog `T.append`
              " bin" ]

makePathVar :: [Text] -> Text
makePathVar = T.intercalate ":"

ghcVersion, cabalVersion, happyVersion, alexVersion :: Text
ghcVersion = "8.0.1"
cabalVersion = "1.24"
happyVersion = "1.19.5"
alexVersion = "3.1.7"
