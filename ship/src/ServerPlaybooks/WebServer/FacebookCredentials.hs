{-# LANGUAGE OverloadedStrings #-}

module ServerPlaybooks.WebServer.FacebookCredentials
    ( FacebookCredentials(..)
    , saveFacebookCredentials
    , loadFacebookCredentials
    ) where

import Data.Aeson
import Data.Text (Text)
import Shelly
import qualified Data.ByteString.Lazy as BL

data FacebookCredentials = FacebookCredentials
    { appName :: Text
    , appId :: Text
    , appSecret :: Text
    }

instance FromJSON FacebookCredentials where
    parseJSON (Object v) =
        FacebookCredentials <$>
            v .: "appName" <*>
            v .: "appId" <*>
            v .: "appSecret"
    parseJSON _ = fail "Not an Object"

instance ToJSON FacebookCredentials where
    toJSON credentials = object
        [ "appName" .= appName credentials
        , "appId" .= appId credentials
        , "appSecret" .= appSecret credentials
        ]

saveFacebookCredentials :: FacebookCredentials -> Shelly.FilePath -> Sh ()
saveFacebookCredentials credentials file =
    writeBinary file (BL.toStrict (encode credentials))

loadFacebookCredentials :: Shelly.FilePath -> Sh FacebookCredentials
loadFacebookCredentials file = do
    bytes <- readBinary file
    case eitherDecodeStrict' bytes of
        Left err -> fail err
        Right x -> pure x
