module Main where

import Automaton
import ServerPlaybooks.DevServer
import ServerPlaybooks.Kibbles
import ServerPlaybooks.Simple1
import ServerPlaybooks.WebServer

main :: IO ()
main = defaultMain
    [ kibbles
    , devServer
    , simple1
    , webServer
    ]
