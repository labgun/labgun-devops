#/bin/bash

set -ex

# Install updates for operating system
apt-get update -y
apt-get upgrade -y

# Useful programs for interactive work
apt-get install -y tmux vim

# Needed packages
apt-get install -y git lxd

# Haskell development tools from: https://launchpad.net/~hvr/+archive/ubuntu/ghc
GHC_VER=7.10.3
CABAL_VER=1.24
add-apt-repository -y ppa:hvr/ghc
apt-get update
apt-get install -y "ghc-$GHC_VER" "cabal-install-$CABAL_VER"

# Enable system swap file
fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab

# Add user for interactive login
useradd -m -s /bin/bash -U captain
usermod -aG lxd captain
echo >> /home/captain/.profile
echo "PATH=/opt/ghc/$GHC_VER/bin:/opt/cabal/$CABAL_VER/bin:"'$PATH' >> /home/captain/.profile

cat << 'EOF' | sudo -u captain -i
  ssh-keygen -t rsa -b 4096 -N '' -C "$HOSTNAME"-$(whoami) -f "$HOME/.ssh/id_rsa"
  cabal update
  git clone https://gitlab.com/labgun/labgun-devops.git
  cd labgun-devops
  cabal new-build
EOF
